import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/post/post';
import { CreatePost } from '../models/post/create-post';
import { User } from '../models/user/user';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public viewAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`http://localhost:3000/posts`);
  }
  public getPostById(id: string): Observable<Post> {
    return this.http.get<Post>(`http://localhost:3000/posts/${id}`);
  }
  public createPost(createPost: CreatePost): Observable<Post> {
    return this.http.post<Post>('http://localhost:3000/posts', createPost);
  }
  public editPost(editPost: {content: string}, id: string): Observable<Post> {
      return this.http.put<Post>(`http://localhost:3000/posts/${id}`, editPost);
  }
  public deletePost(postId: string): Observable<Post> {
    return this.http.delete<Post>(`http://localhost:3000/posts/${postId}`);
}
}
