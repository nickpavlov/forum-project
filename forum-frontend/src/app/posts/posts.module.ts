import { AllPostsComponent } from './all-posts/all-posts.component';
import { NgModule } from '@angular/core';
import { PostsService } from './posts.service';
import { MaterialModule } from '../material/material.module';
import { ListPostComponent } from './list-post/list-post.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { PostsRoutingModule } from './posts-routing.module';
import { CommonModule } from '@angular/common';
import { AllCommentsComponent } from '../comments/all-comments/all-comments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommentsModule } from '../comments/comments.module';
import { CommentDetailsComponent } from '../comments/comment-details/comment-details.component';
import { CommentsRoutingModule } from '../comments/comments-routing.module';

@NgModule({
    declarations: [
      AllCommentsComponent,
      AllPostsComponent,
      ListPostComponent,
      PostDetailsComponent,
      AllCommentsComponent,
      CommentDetailsComponent
        ],
    imports: [
      ReactiveFormsModule,
      FormsModule,
      PostsRoutingModule,
      MaterialModule,
      CommonModule,
      CommentsModule,
      CommentsRoutingModule
    ],
    providers: [PostsService]
  })
  export class PostsModule { }
