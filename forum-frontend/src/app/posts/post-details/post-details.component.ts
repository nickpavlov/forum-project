import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from 'src/app/models/post/post';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsService } from '../posts.service';
import { User } from 'src/app/models/user/user';
import { NotificationService } from '../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {

  public loggedUser: User;
  public subcribedUser: Subscription;
  public post: Post;
  public isToggled = false;

  public input: string;


  constructor(
    private readonly authService: AuthService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly postsService: PostsService,

    private readonly router: Router,

    private readonly notificator: NotificationService
  ) {}

  ngOnInit(): void {
    this.subcribedUser = this.authService.loggedUserData$.subscribe((res: User) => {
      this.loggedUser = {...res};
    });

    const postId: string = this.activatedRoute.snapshot.params[`id`];
    this.postsService
    .getPostById(postId)
    .subscribe((postToView: Post) => (this.post = postToView));
  }

  public isAuthorOrAdmin(postAuthor: string): boolean {
    return this.loggedUser.username === postAuthor || this.loggedUser.role === 'admin';
  }

  public toggleSwitch(): boolean {
    return this.isToggled = !this.isToggled;
  }

  public editPostDescription(updateContent: string): void {

    this.postsService.editPost({ content: updateContent }, this.post.id).subscribe((res) => {
      this.post.content = res.content;
      this.notificator.success('Post description updated');
    });
  }
  public deletePost(): void {
    this.postsService.deletePost(this.post.id).subscribe(() => {
      this.notificator.success('Post deleted');
      this.router.navigate(['posts']);
    });
  }
}
