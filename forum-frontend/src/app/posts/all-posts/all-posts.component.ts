import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Post } from 'src/app/models/post/post';
import { CreatePost } from 'src/app/models/post/create-post';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user/user';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.scss']
})
export class AllPostsComponent implements OnInit {


  public subcribedUser: Subscription;

  public loggedUser: User;

  public title: string;
  public description: string;
  public show = false;
  constructor(
    private readonly postsService: PostsService,
    private readonly authService: AuthService,
    private readonly notificator: NotificationService
    ) { }

  public posts: Post[];

  ngOnInit(): void {
    this.subcribedUser = this.authService.loggedUserData$.subscribe(res => this.loggedUser = res);
    this.postsService.viewAllPosts().subscribe(
      (data: Post[]) => {
        this.posts = data;
      },
      console.error,
    );
  }
  toggleButton() {
    this.show = !this.show;
  }

  createPost() {
    const newPost: CreatePost = {
      title: this.title,
      content: this.description,
    };
    this.postsService.createPost(newPost).subscribe((res: any) => {
      this.posts.push(res);
      this.notificator.success('Post was created!');
    });
    this.show = false;
  }
}
