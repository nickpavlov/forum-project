import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Post } from 'src/app/models/post/post';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user/user';

@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.scss']
})
export class ListPostComponent {

  @Input()
  public post: Post;



  constructor(
    private readonly router: Router
  ) {}

  public previewPost(): void {
    this.router.navigate(['posts', this.post.id]);
  }

}
