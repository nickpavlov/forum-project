import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule,  } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { CommentsRoutingModule } from './comments-routing.module';
import { CommentService } from './service/comments.service';



@NgModule({
  declarations: [],
  imports: [
    FormsModule,
    CommonModule,
    MaterialModule,
    CommentsRoutingModule
  ],
  providers: [CommentService]
})
export class CommentsModule { }
