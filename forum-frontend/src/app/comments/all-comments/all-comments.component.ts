import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../service/comments.service';
import { Comment } from '../../models/comment/comments';
import { AddComment } from '../../models/comment/add-comment';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { Post } from 'src/app/models/post/post';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-all-comments',
  templateUrl: './all-comments.component.html',
  styleUrls: ['./all-comments.component.scss']
})
export class AllCommentsComponent implements OnInit {

  @Input()
  post: Post;
  @Input()
  public comments: Comment[];

  @Input()
  loggedUser: User;

  public subscription: Subscription;

  public input: string;
  constructor(
    private readonly commentService: CommentService,
    private readonly notificator: NotificationService,
    private readonly router: Router
  ) {}

  ngOnInit() {
   return this.comments;
  }


  createComment(): void {
    const newComment: AddComment = {
      content: this.input
    };
    // this.input = '';
    this.commentService.createComment(newComment, this.post.id).subscribe((res: Comment) => {
      this.comments.push(res);

    });
  }

  editComment(editedComment: {content: string, commentId: string}): void {
    this.commentService.editComment({content: editedComment.content}, this.post.id, editedComment.commentId).subscribe((res) => {

      const editCommentId: AddComment = this.comments.find((comment) => comment.id === editedComment.commentId);
      editCommentId.content = res.content;
    });
  }

  deleteComment(commentId: string): void {
    this.commentService.deleteComment(this.post.id, commentId).subscribe(() => {
      this.comments = this.comments.filter((comment) => comment.id !== commentId);
      this.notificator.success(`Comment was deleted.`);
      this.router.navigate([`posts/${this.post.id}`]);
    });
  }

}
