import { Injectable } from '@angular/core';
import { AddComment } from '../../models/comment/add-comment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
      private readonly http: HttpClient,
  ) {}

  public createComment(newComment: AddComment, id: string): Observable<AddComment> {
      return this.http.post<AddComment>(`http://localhost:3000/posts/${id}/comments`, newComment);
  }

  public editComment(editComment: AddComment, id: string, commentId: string): Observable<AddComment> {
      return this.http.put<AddComment>(`http://localhost:3000/posts/${id}/comments/${commentId}`, editComment);
  }

  public deleteComment(postId: string, commentId: string): Observable<AddComment> {
      return this.http.delete<AddComment>(`http://localhost:3000/posts/${postId}/comments/${commentId}`);
  }
}
