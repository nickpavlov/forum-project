import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../core/guards/auth.guard';
import { AllCommentsComponent } from './all-comments/all-comments.component';
import { CommentDetailsComponent } from './comment-details/comment-details.component';



const routes: Routes = [
  { path: '', component: AllCommentsComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: ':id', component: CommentDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }
