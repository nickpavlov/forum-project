import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Comment } from '../../models/comment/comments';
import { User } from 'src/app/models/user/user';
import { NotificationService } from '../../core/services/notification.service';
import { Post } from 'src/app/models/post/post';

@Component({
  selector: 'app-comment-details',
  templateUrl: './comment-details.component.html',
  styleUrls: ['./comment-details.component.scss']
})
export class CommentDetailsComponent implements OnInit {

  @Input()
  public post: Post;

  @Input()
  public comment: Comment;
  @Input()
  public loggedUser: User;
  public viewEdit: boolean;
  public input: string;
  @Output()
  public editEmitter = new EventEmitter();
  @Output()
  public deleteEmitter = new EventEmitter();

  constructor(
    private readonly notificator: NotificationService,
  ) { }

  ngOnInit(): void {

    this.input = this.comment.content;
  }

  toggleViewEdit(): void {
    this.viewEdit = !this.viewEdit;
  }

  isAuthorOrAdmin(commentAuthor: string): boolean {
    return this.loggedUser.username === commentAuthor || this.loggedUser.role === 'admin';
  }

  editComment(): void {
    this.editEmitter.emit(
      {content: this.input, commentId: this.comment.id}
    );
    this.toggleViewEdit();
  }
  deleteComment() {
    this.deleteEmitter.emit(this.comment.id);

  }
}
