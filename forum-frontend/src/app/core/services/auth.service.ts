import { UserCredentials } from 'src/app/models/user/user-credentials';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<User>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService
  ) {}

  public get loggedUserData$(): Observable<User> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: User): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): User {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }


  public login(user: UserCredentials): Observable<any> {
    return this.http
      .post<UserCredentials>(`http://localhost:3000/auth/login`, user)
      .pipe(
        tap(({ accessToken }) => {
          this.storage.setItem('token', accessToken);
          const userData: User = this.jwtService.decodeToken(accessToken);
          this.emitUserData(userData);
        })
      );
  }
  public logout(): Observable<any> {
    return this.http.delete(`http://localhost:3000/auth/logout`)
    .pipe(
      tap(() => {
        this.storage.removeItem('token');
        this.emitUserData(null);
      })
    );
  }
}
