import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) {}

  public canActivate(): boolean {
    if (!this.authService.getUserDataIfAuthenticated()) {
      this.notificationService.error(
        `You're not logged in!`
      );
      this.router.navigate(['user/login']);

      return false;
    }

    return true;
  }
}
