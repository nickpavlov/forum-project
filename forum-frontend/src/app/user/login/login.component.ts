import { Component, OnInit } from '@angular/core';
import { UserCredentials } from 'src/app/models/user/user-credentials';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  public constructor(
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {}

  public ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  public login(user: UserCredentials) {
    this.authService.login(user).subscribe(
      () => {
        this.notification.success(`Successful login!`);
        this.router.navigate(['/home']);
      },
      () => {
        this.notification.error('Login failed!');
      }
    );
  }
}

