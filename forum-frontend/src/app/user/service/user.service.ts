import { UserCredentials } from 'src/app/models/user/user-credentials';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public constructor(
    private readonly http: HttpClient,
  ) {}

  public register(user: UserCredentials): Observable<UserCredentials> {
    return this.http.post<UserCredentials>(
      `http://localhost:3000/users/register`,
      user
    );
  }
}
