import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Router } from '@angular/router';
import { UserCredentials } from 'src/app/models/user/user-credentials';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;

  public constructor(
    private readonly userService: UserService,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {}

  public ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16)
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
        ]
      ],
      email: [
        '',
        [
        Validators.required,
        Validators.email
        ]
      ],
      firstname: [
        '',
        Validators.required,
        Validators.maxLength(20)
      ],
      lastname: [
        '',
        [
        Validators.required,
        Validators.maxLength(20)
        ]
      ]
    });
  }

  public register(user: UserCredentials) {
    this.userService.register(user).subscribe(
      () => {
        this.notification.success('Successful register!');
        this.router.navigate(['/users/login']);
      },
      () => {
        this.notification.error('Registration failed!');
      }
    );
  }
}
