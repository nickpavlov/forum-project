import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user/user';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public loggedUserData: User;
  private loggedUserSubscription: Subscription;

  public constructor(
    public readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: User) => {
        this.loggedUserData = data;
      });
  }


  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public logout(): void {
    this.authService
      .logout()
      .subscribe(
        () => this.notification.success('Successful logout!'),
        () => this.notification.error('Logout failed!')
      );
    this.router.navigate(['home']);
  }
}
