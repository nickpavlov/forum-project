export interface BanStatus {
  id: string;
  user: string;
  isBanned?: boolean;
  description?: string;
  expDate?: Date;
}
