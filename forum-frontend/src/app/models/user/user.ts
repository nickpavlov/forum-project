import { UserRole } from '../enums/user-roles.enum';
import { BanStatus } from '../ban-status/ban-status';

export interface User {
  id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  registered: Date;
  role: UserRole;
  banStatus: BanStatus;
}
