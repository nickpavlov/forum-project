export interface Comment {
    id: string;

    content: string;

    dateCreated: Date;

    votes: number;

    usersVoted: string[];

    editor: string;
}
