

export interface Post {
    id: string;

    title: string;

    content: string;

    author: string;

    dateCreated: Date;

    dateUpdated: Date;

    editor: string;

    __comments__: Comment[];

    isDeleted: boolean;

}
