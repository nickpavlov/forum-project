import { BanStatus } from '../../../database/entities/ban.entity';
export interface IJWTPayload {
    username: string;

    banStatus: BanStatus;

}