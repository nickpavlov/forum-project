import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1586513320053 implements MigrationInterface {
    name = 'Initial1586513320053'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `comments` CHANGE `editor` `editor` varchar(255) NULL DEFAULT NULL", undefined);
        await queryRunner.query("ALTER TABLE `ban_status` CHANGE `description` `description` varchar(255) NULL DEFAULT NULL", undefined);
        await queryRunner.query("ALTER TABLE `ban_status` CHANGE `expDate` `expDate` date NULL DEFAULT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `ban_status` CHANGE `expDate` `expDate` date NULL", undefined);
        await queryRunner.query("ALTER TABLE `ban_status` CHANGE `description` `description` varchar(255) NULL", undefined);
        await queryRunner.query("ALTER TABLE `comments` CHANGE `editor` `editor` varchar(255) NULL", undefined);
    }

}
