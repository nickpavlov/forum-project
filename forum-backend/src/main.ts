import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { ConfigService } from '@nestjs/config';
import helmet = require('helmet');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  // app.use(helmet());

  await app.listen(+app.get(ConfigService).get('PORT'));
}

bootstrap();
